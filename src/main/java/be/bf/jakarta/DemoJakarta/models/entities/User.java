package be.bf.jakarta.DemoJakarta.models.entities;

import be.bf.jakarta.DemoJakarta.models.PayType;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "Security_User")
public class User extends BaseEntity<Long> {

    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String email;

    @Embedded
    private Address address;

    @Enumerated(EnumType.STRING)
    private PayType defaultPayType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PayType getDefaultPayType() {
        return defaultPayType;
    }

    public void setDefaultPayType(PayType defaultPayType) {
        this.defaultPayType = defaultPayType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", defaultPayType=").append(defaultPayType);
        sb.append(", isActive=").append(isActive);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append('}');
        return sb.toString();
    }

//    @Override
//    @PrePersist()
//    public void prePersist() {
//        this.createdAt = LocalDate.now();
//    }
//
//    @Override
//    @PreUpdate()
//    public void preUpdate() {
//        this.updatedAt = LocalDate.now();
//    }
}
