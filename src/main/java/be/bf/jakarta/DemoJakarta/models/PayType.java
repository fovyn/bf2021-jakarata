package be.bf.jakarta.DemoJakarta.models;

public enum PayType {
    BANCONTACT,
    VISA,
    MASTERCARD,
    PAYPAL,
    MAESTRO,
    CASH
}
