package be.bf.jakarta.DemoJakarta.services;

import be.bf.jakarta.DemoJakarta.models.entities.Command;
import be.bf.jakarta.DemoJakarta.models.entities.UserRole;
import jakarta.ejb.Singleton;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

import java.util.List;

@Singleton
public class CommandService {
//    @PersistenceContext(unitName = "javaee-demo")
    private EntityManager manager;

    public List<Command> findAll(){
        TypedQuery<Command> query = manager.createQuery("SELECT c FROM Command c WHERE c.isActive = true", Command.class);
        TypedQuery<UserRole> urQuery = manager.createQuery("SELECT ur FROM UserRole ur JOIN ur.role r WHERE ur.user = :user", UserRole.class);

        return query.getResultList();
    }
}
