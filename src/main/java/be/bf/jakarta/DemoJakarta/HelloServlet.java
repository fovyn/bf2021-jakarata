package be.bf.jakarta.DemoJakarta;

import java.io.*;
import java.util.List;

import be.bf.jakarta.DemoJakarta.models.entities.Command;
import be.bf.jakarta.DemoJakarta.services.CommandService;
import com.google.gson.Gson;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
//    @Inject
//    private ClientService service;
    @Inject
    private CommandService commandService;

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");

//        List<Client> clients = service.findAll();

        List<Command> commands = commandService.findAll();
        PrintWriter writer = response.getWriter();

//        writer.println(new Gson().toJson(clients));
        writer.println(new Gson().toJson(commands));
    }


    public void destroy() {
    }
}